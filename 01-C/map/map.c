#include <stdio.h>

// Definieren Sie ein enum cardd
enum cardd
{
	N = 1,
	S = 2 << 0,
	W = 2 << 1,
	E = 2 << 2
};
// Definieren Sie ein 3x3-Array namens map, das Werte vom Typ cardd enthält

enum cardd map[3][3];

// Die Funktion set_dir soll an Position x, y den Wert dir in das Array map eintragen
// Überprüfen Sie x und y um mögliche Arrayüberläufe zu verhindern
// Überprüfen Sie außerdem dir auf Gültigkeit
void set_dir(int x, int y, enum cardd dir)
{
	//Überprüfe 0 <= xy < 3 (Länge des Arrays) 
	if (x < 3 && x >= 0 && y < 3 && y >= 0)
		//Überprüfe dir, so dass nicht Nord und Süd bzw. Ost und West gleichzeitig "wahr" sein können
		//und dir mindestens eine Himmelsrichtung ist
		if (!(dir & N && dir & S || dir & E && dir & W || (dir && !dir & (N | E | S | W))))
			map[y][x] = dir;
}

// Die Funktion show_map soll das Array in Form einer 3x3-Matrix ausgeben
void show_map(void)
{
	for (int y = 0; y < 3; y++)
	{
		for (int x = 0; x < 3; x++)
		{
			int two = map[x][y] & (N | S) && map[x][y] & (W | E);
			if (x == 1)
				printf("_");
			else if (x == 2)
				if (two)
					printf("_");
				else
					printf("__");
			switch (map[x][y])
			{
			case N:
				printf("N");
				break;
			case N | E:
				printf("NE");
				break;
			case E:
				printf("E");
				break;
			case S | E:
				printf("SE");
				break;
			case S:
				printf("S");
				break;
			case S | W:
				printf("SW");
				break;
			case W:
				printf("W");
				break;
			case N | W:
				printf("NW");
				break;

			default:
				printf("0");
				break;
			}
			if (x == 1 && !two)
				printf("_");

			else if (x == 0)
				if (two)
					printf("_");
				else
					printf("__");
		}
		printf("\n");
	}
}

int main(void)
{
	// In dieser Funktion darf nichts verändert werden!
	set_dir(0, 1, N);
	set_dir(1, 0, W);
	set_dir(1, 4, W);
	set_dir(1, 2, E);
	set_dir(2, 1, S);

	show_map();

	set_dir(0, 0, N | W);
	set_dir(0, 2, N | E);
	set_dir(0, 2, N | S);
	set_dir(2, 0, S | W);
	set_dir(2, 2, S | E);
	set_dir(2, 2, E | W);
	set_dir(1, 3, N | S | E);
	set_dir(1, 1, N | S | E | W);

	show_map();

	return 0;
}

---
geometry: margin=2cm
output: pdf_document
---

# Abgabe 4

## Parallelisierung mit OpenMP

Nach der Anpassung von Makefile und pardiff.c, zur zeilenweisen Parallelisierung, d.h. jeder Thread berechnet eine Zeile bis alle Zeilen berechnet sind, konnten wir bei einer Messung mit 12 Threads, den geforderten Beschleunigungsfaktor von 10 feststellen.

Zeiten in s bei 512 Interlines und 1000 Iterationen:

Sequenziell | Parallel
----------- | -------- 
531.28      | 50.18   
527.61      | 50.49
528.92      | 58.16
527.55      | 55.41   

Wobei die Tests alternierend auf einem Knoten ausgeführt wurden um möglichst akkurate Testergebnisse zu erhalten. Die durchschnittliche Dauer wurde so um einen Faktor von 9.88 reduziert.

Anlesen ca. 30min, Umsetzung 45min, Fehlersuche und Messung 60min

## Umsetzung der Datenaufteilungen
Mit Hilfe von `#ifdef`-Makros, und definieren von den Variablen `OMP`, `SPALTEN` und `ELEMENT` im Makefile, haben wir alternative Codeblöcke, um die unterschiedlichen Algorithmen umzusetzen, wie z.B. die Umsortierung der `for`-Schleifen für die zeilen-/spaltenweise Parallelisierung.
 
![la lune](aufteilung.svg){ width=60% }

Obwohl Zeilenweise schneller ist als Spaltenweise, ist der Unterschied nicht besonders groß, ca. 10s. Der Unterschied hier entsteht aus dem gleichen Grund wie beim Sequenziellen Code, da zeilenweise der Prozessorcache besser genutzt wird als spaltenweise.

Teilt man jedoch die Daten elementweise auf, war der parallele Code teilweise sogar langsamer als der sequenzielle. Auch fällt auf, das die Laufzeit deutlich unterschiedlich ist, was zum einen an der insgesamt längeren Laufzeit liegt, wodurch kleinere Unterschiede der Prozessorlast sich deutlicher wiederspiegeln, aber auch an der ungünstigen Parallelisierung, wodurch sich die Threads regelmäßig gegenseitig blockieren um z.B. `max_residuum` zu schreiben, ein Prozess, der nicht deterministisch ist.

## Leistungsanalyse

### Messung 1
In der ersten Messung, haben wir die Leistungssteigerung bei wachsender Threadzahl verglichen.

Dafür haben wir 512 Interlines bei 1000 Iterationen verwendet.

![la lune](speedup.svg){ width=70% }

Hier sieht man, dass der Jacobi-Algorithmus sehr gut parallelisierbar ist. Da jede Zeile unabhängig berechnet werden kann und daher nur das Erzeugen der Matrix und die Rückgabe sequenziell laufen. Beides im Vergleich zur Berechnung schnelle Prozesse.
Je schneller die `calculate()` Funktion jedoch ausgeführt wird, desto größer wird der Anteil, den diese sequenziellen Prozesse ausmachen.

Bei zusätzlichen Messungen die über 12 Threads hinaus gehen würden, weicht daher der gemessene Wert weiter von der optimalen Laufzeit ab, bei 24 Threads hatten wir nur noch 94% Effizienz. Der worst-case sind dann mehr Kerne als Interlines, was mit dem Beispiel aus der Vorlesung von 1000 Bauarbeitern und einem Loch vergleichbar ist, da dann nicht genug Zeilen da sind, damit jeder Prozess rechnen kann.
\newpage

### Messung 2
Anschließend haben wir die Laufzeit unter Änderung der Interlines betrachtet, mit konstanter Threadzahl von 12.

![la lune](speedup_copy.svg){ width=70% }

Hierbei fällt auf, das die Dauer bei $2^0=1$ bis $2^6=64$ nahezu Konstant bleibt. Dies ist einfach zu erklären, da hier die `for`-Schleifen in `calculate()` besonders bei Multithreading nur sehr selten $\frac{64}{12}=5,\overline{3}$ nacheinander ausgeführt werden. Das exponentielle Wachstum rührt daher, dass die X-Achse logarithmisch skaliert ist. Andernfalls hat man ein quadratisches Wachstum, denn mit Verdopplung der Interlines, vervierfacht sich die Größe der Matrix. 

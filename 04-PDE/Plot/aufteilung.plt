set term pdf;
set output "aufteilung.pdf"
set xrange [-0.5 : 2.5]
unset xtics

set xlabel "Datenaufteilung"
set ylabel "Relative Dauer"
set xtics format " "
set xtics ("Elementweise" 0, "Spaltenweise" 1, "Zeilenweise"  2)
set style data histograms
 set style fill solid
 set boxwidth 0.9
 plot "0.txt" using ($1)/1321.14 notitle lt rgb "#406090",\
      "1.txt" using ($1)/1321.14 notitle lt rgb "#406090", \
      "2.txt" using ($1)/1321.14 notitle lt rgb "#406090"

#!/usr/bin/env gnuplot

set term svg;
set termopt enhanced

# Datei, aus der gelesen wird
in_file="laufzeiten.txt"



# Lade Eingabedatei in datablock $data (hackiger Hack)
set table $data
   plot in_file using (($1+$2+$3)/3) with table
unset table

# Speichere Wert für 1 Thread in first_time

# stats liest eine Menge Infos aus $data und speichert sie in Variablen STATS_*
stats $data nooutput

# Maximale Anzahl Threads wird als Anzahl Records aus stats gesetzt
max_threads = STATS_records
max=STATS_max
# Falls in der ersten Spalte der größte Wert ist, setze unteres Limit der x-Achse
# auf 1, ansonsten auf 0.

# Falls wir irgendwo superlinearen Speedup haben, setze ymax dementsprechend

# Setze Grenzen von x- und y-Achse

set xrange [1 : max_threads-1]
set yrange [-0.05 : 1]
# Setze Position der Achsen-Tics
set xtics 1,1,max_threads

set xlabel "log_2(#Interlines)"
set ylabel "Dauer"

# Legende Oben Links
set key left top

# Ausgabedatei
set output "../../speedup_copy.svg"

f(x) = x

# $0 ist die nullbasierte Zeilennummer
# $0 + 1 ist die 1-basierte Zeilennummer (= Anzahl Threads)
# $1 ist der Wert in der ersten (und einzigen) Spalte
# first_time / $1 ist der Speedup
# Plotte die Anzahl Threads gegen den Speedup, und den Optimalen Speedup
plot "$data" using ($0):($1/max) with linespoints lt rgb "#FF0000" notitle,

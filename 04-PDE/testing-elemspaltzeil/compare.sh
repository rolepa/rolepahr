#!/bin/bash
for ((var=0; var <= 3; var++))
do
    echo testing element
    time ../partdiff-openmp-element 12 2 512 2 2 1000 >>element.test
    echo testing spalten
    time ../partdiff-openmp-spalten 12 2 512 2 2 1000 >>spalten.test
    echo testing zeilen
    time ../partdiff-openmp-zeilen 12 2 512 2 2 1000 >>zeilen.test
done
#!/bin/bash
name=$(hostname --short);
mkdir $name
echo $name >>"$name/timing.test"
echo $name >>"$name/output.test"
for ((var=1; var <= 1024; var=var*2))
do
    echo testing $var 
    echo -n testing $var >>"$name/timing.test"
    { time ../partdiff-openmp 12 2 $var 2 2 300 >>"$name/output.test";} 2>>"$name/timing.test"
    echo  >>"$name/timing.test"
done
#include <omp.h>

#include <stdio.h>
#include <stdlib.h>

int x = 0;
// easy solution would be just to devide the array first and then start the threads
int get()
{
    int i = x;
    int y = x * i;
    x = i + 1;
    return x;
}

int main(int argc, char **argv)
{
    int thread_id, nthreads;

#pragma omp parallel
#pragma omp for
    for (int i = 0; i < 10; ++i)
        printf("thread%d, %d\n", omp_get_thread_num(), i);

#pragma omp parallel
    {
        
        #pragma omp critical
        printf("test%d\n", get());

#pragma omp barrier
    }
    if (thread_id == 0)
    {
        nthreads = omp_get_num_threads();
        printf("There are %d threads\n", x / nthreads);
    }

    return EXIT_SUCCESS;
}

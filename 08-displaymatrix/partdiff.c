/****************************************************************************/
/****************************************************************************/
/**                                                                        **/
/**                 TU München - Institut für Informatik                   **/
/**                                                                        **/
/** Copyright: Prof. Dr. Thomas Ludwig                                     **/
/**            Andreas C. Schmidt                                          **/
/**                                                                        **/
/** File:      partdiff.c                                                  **/
/**                                                                        **/
/** Purpose:   Partial differential equation solver for Gauß-Seidel and    **/
/**            Jacobi method.                                              **/
/**                                                                        **/
/****************************************************************************/
/****************************************************************************/

/* ************************************************************************ */
/* Include standard header file.                                            */
/* ************************************************************************ */
#define _POSIX_C_SOURCE 200809L

#include <inttypes.h>
#include <malloc.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "partdiff.h"

struct calculation_arguments {
    uint64_t N;            /* number of spaces between lines (lines=N+1)     */
    uint64_t num_matrices; /* number of matrices                             */
    double h;              /* length of a space between two lines            */
    double*** Matrix;      /* index matrix used for addressing M             */
    double* M;             /* two matrices with real values                  */
    int rank;              /* rank of prozess                                */
    int size;              /* number of prozesses                            */
    int from;              /* first line calculated by prozess               */
    int to;                /* last line calculated by prozess                */
    uint64_t height;       /* number of lines stored in matrix               */
};

struct calculation_results {
    uint64_t m;
    uint64_t stat_iteration; /* number of current iteration                    */
    double stat_precision;   /* actual precision of all slaves in iteration    */
};

/* ************************************************************************ */
/* Global variables                                                         */
/* ************************************************************************ */

/* time measurement variables */
struct timeval start_time; /* time when program started                      */
struct timeval comp_time;  /* time when calculation completed                */

/* ************************************************************************ */
/* initVariables: Initializes some global variables                         */
/* ************************************************************************ */
static void initVariables(struct calculation_arguments* arguments,
                          struct calculation_results* results, struct options const* options) {
    arguments->N = (options->interlines * 8) + 9 - 1;
    arguments->num_matrices = (options->method == METH_JACOBI) ? 2 : 1;
    arguments->h = 1.0 / arguments->N;
    unsigned const rank = arguments->rank;
    unsigned const size = arguments->size;
    uint64_t mod = (arguments->N - 1) % size;
    uint64_t div = (arguments->N - 1) / size;
    // berechnet benötigten Platz für matrix
    arguments->height = div + (mod > rank) + 2;
    // berechnet start und ende vom Prozess
    arguments->from = div * rank + (mod >= rank ? rank + 1 : mod + 1);
    arguments->to = arguments->from + arguments->height - 3;

    results->m = 0;
    results->stat_iteration = 0;
    results->stat_precision = 0;
}

/* ************************************************************************ */
/* freeMatrices: frees memory for matrices                                  */
/* ************************************************************************ */
static void freeMatrices(struct calculation_arguments* arguments) {
    uint64_t i;

    for (i = 0; i < arguments->num_matrices; i++) {
        free(arguments->Matrix[i]);
    }

    free(arguments->Matrix);
    free(arguments->M);
}

/* ************************************************************************ */
/* allocateMemory ()                                                        */
/* allocates memory and quits if there was a memory allocation problem      */
/* ************************************************************************ */
static void* allocateMemory(size_t size) {
    void* p;

    if ((p = malloc(size)) == NULL) {
        printf("Speicherprobleme! (%" PRIu64 " Bytes angefordert)\n", size);
        exit(1);
    }

    return p;
}

/* ************************************************************************ */
/* allocateMatrices: allocates memory for matrices                          */
/* ************************************************************************ */
static void allocateMatrices(struct calculation_arguments* arguments) {
    uint64_t i, j;

    uint64_t const N = arguments->N;
    uint64_t const height = arguments->height;
    // allocates needed storage for matrices
    arguments->M = allocateMemory(arguments->num_matrices * height * (N + 1) * sizeof(double));
    arguments->Matrix = allocateMemory(arguments->num_matrices * sizeof(double**));

    for (i = 0; i < arguments->num_matrices; i++) {
        arguments->Matrix[i] = allocateMemory(height * sizeof(double*));

        for (j = 0; j < height; j++) {
            arguments->Matrix[i][j] = arguments->M + (i * height * (N + 1)) + (j * (N + 1));
        }
    }
}

/* ************************************************************************ */
/* initMatrices: Initialize matrix/matrices and some global variables       */
/* ************************************************************************ */
static void initMatrices(struct calculation_arguments* arguments, struct options const* options) {
    uint64_t g, i, j; /*  local variables for loops   */

    uint64_t const N = arguments->N;
    double const h = arguments->h;
    double*** Matrix = arguments->Matrix;

    /* initialize matrix/matrices with zeros */
    for (g = 0; g < arguments->num_matrices; g++) {
        for (i = 0; i < arguments->height; i++) {
            for (j = 0; j <= N; j++) {
                Matrix[g][i][j] = 0.0;
            }
        }
    }

    /* initialize borders, depending on function (function 2: nothing to do) */
    if (options->inf_func == FUNC_F0) {
        for (g = 0; g < arguments->num_matrices; g++) {
            // Initialisierung des Randes, auf mehrere Prozesse aufgeteilt
            for (i = 0; !arguments->rank && i <= N; i++)
                Matrix[g][0][i] = 1.0 - (h * i);
            for (i = 0; arguments->rank == arguments->size - 1 && i <= N; i++)
                Matrix[g][arguments->height - 1][i] = h * i;
            for (int i = 0, i_ = arguments->from - 1; i_ <= arguments->to + 1; i++, i_++) {
                Matrix[g][i][0] = 1.0 - (h * i_);
                Matrix[g][i][N] = h * i_;
            }
            if (arguments->rank == arguments->size - 1)
                Matrix[g][arguments->height - 1][0] = 0.0;
            if (!arguments->rank)
                Matrix[g][0][N] = 0.0;
        }
    }
}

/* ************************************************************************ */
/* calculate: solves the equation                                           */
/* ************************************************************************ */
static void calculate_parallel_jacobi(struct calculation_arguments const* arguments,
                      struct calculation_results* results, struct options const* options) {
    int i, j;           /* local variables for loops */
    int m1, m2;         /* used as indices for old and new matrices */
    double star;        /* four times center value minus 4 neigh.b values */
    double residuum;    /* residuum of current iteration */
    double maxresiduum; /* maximum residuum value of a slave in iteration */
    int rank = arguments->rank;
    int size = arguments->size;
    int const N = arguments->N;
    int const height = arguments->height;
    double const h = arguments->h;

    double pih = 0.0;
    double fpisin = 0.0;

    int term_iteration = options->term_iteration;

    /* initialize m1 and m2 depending on algorithm */
    if (options->method == METH_JACOBI) {
        m1 = 0;
        m2 = 1;
    } else {
        m1 = 0;
        m2 = 0;
    }

    if (options->inf_func == FUNC_FPISIN) {
        pih = PI * h;
        fpisin = 0.25 * TWO_PI_SQUARE * h * h;
    }

    while (term_iteration > 0) {
        double** Matrix_Out = arguments->Matrix[m1];
        double** Matrix_In = arguments->Matrix[m2];

        maxresiduum = 0;

        /* over all rows in prozess*/
        for (i = 1; i < height - 1; i++) {
            double fpisin_i = 0.0;

            if (options->inf_func == FUNC_FPISIN) {
                // needs to use the global line index
                fpisin_i = fpisin * sin(pih * (double)(i + arguments->from - 1));
            }

            /* over all columns */
            for (j = 1; j < N; j++) {
                star = 0.25 * (Matrix_In[i - 1][j] + Matrix_In[i][j - 1] + Matrix_In[i][j + 1] +
                               Matrix_In[i + 1][j]);

                if (options->inf_func == FUNC_FPISIN) {
                    star += fpisin_i * sin(pih * (double)j);
                }

                if (options->termination == TERM_PREC || term_iteration == 1) {
                    residuum = Matrix_In[i][j] - star;
                    residuum = (residuum < 0) ? -residuum : residuum;
                    maxresiduum = (residuum < maxresiduum) ? maxresiduum : residuum;
                }

                Matrix_Out[i][j] = star;
            }
        }

        results->stat_iteration++;
        // Synchronisiert maxresiduum
        const double maxresconst = maxresiduum;
        MPI_Allreduce(&maxresconst, &maxresiduum, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
        results->stat_precision = maxresiduum;
        /* exchange m1 and m2 */
        i = m1;
        m1 = m2;
        m2 = i;

        /* check for stopping calculation depending on termination method */
        if (options->termination == TERM_PREC) {
            if (maxresiduum < options->term_precision) {
                term_iteration = 0;
            }
        } else if (options->termination == TERM_ITER) {
            term_iteration--;
        }
        MPI_Barrier(MPI_COMM_WORLD);
        if (term_iteration) {
            MPI_Request req;
            // Schickt obere und untere Zeile an nachbarprozesse
            if (rank)
                MPI_Isend(Matrix_Out[1] + 1, N, MPI_DOUBLE, rank - 1, 0, MPI_COMM_WORLD, &req);
            if (rank != size - 1)
                MPI_Isend(Matrix_Out[height - 2] + 1, N, MPI_DOUBLE, rank + 1, 0, MPI_COMM_WORLD,
                          &req);
            // und empfängt
            if (rank)
                MPI_Recv(Matrix_Out[0] + 1, N, MPI_DOUBLE, rank - 1, 0, MPI_COMM_WORLD,
                         MPI_STATUS_IGNORE);
            if (rank != size - 1)
                MPI_Recv(Matrix_Out[height - 1] + 1, N, MPI_DOUBLE, rank + 1, 0, MPI_COMM_WORLD,
                         MPI_STATUS_IGNORE);
        }
    }

    results->m = m2;
}

/* ************************************************************************ */
/* calculate: solves the equation                                           */
/* ************************************************************************ */
static void calculate_old(struct calculation_arguments const* arguments,
                          struct calculation_results* results, struct options const* options) {
    int i, j;           /* local variables for loops */
    int m1, m2;         /* used as indices for old and new matrices */
    double star;        /* four times center value minus 4 neigh.b values */
    double residuum;    /* residuum of current iteration */
    double maxresiduum; /* maximum residuum value of a slave in iteration */

    int const N = arguments->N;
    double const h = arguments->h;

    double pih = 0.0;
    double fpisin = 0.0;

    int term_iteration = options->term_iteration;

    /* initialize m1 and m2 depending on algorithm */
    if (options->method == METH_JACOBI) {
        m1 = 0;
        m2 = 1;
    } else {
        m1 = 0;
        m2 = 0;
    }

    if (options->inf_func == FUNC_FPISIN) {
        pih = PI * h;
        fpisin = 0.25 * TWO_PI_SQUARE * h * h;
    }

    while (term_iteration > 0) {
        double** Matrix_Out = arguments->Matrix[m1];
        double** Matrix_In = arguments->Matrix[m2];

        maxresiduum = 0;

        /* over all rows */
        for (i = 1; i < N; i++) {
            double fpisin_i = 0.0;

            if (options->inf_func == FUNC_FPISIN) {
                fpisin_i = fpisin * sin(pih * (double)i);
            }

            /* over all columns */
            for (j = 1; j < N; j++) {
                star = 0.25 * (Matrix_In[i - 1][j] + Matrix_In[i][j - 1] + Matrix_In[i][j + 1] +
                               Matrix_In[i + 1][j]);

                if (options->inf_func == FUNC_FPISIN) {
                    star += fpisin_i * sin(pih * (double)j);
                }

                if (options->termination == TERM_PREC || term_iteration == 1) {
                    residuum = Matrix_In[i][j] - star;
                    residuum = (residuum < 0) ? -residuum : residuum;
                    maxresiduum = (residuum < maxresiduum) ? maxresiduum : residuum;
                }

                Matrix_Out[i][j] = star;
            }
        }

        results->stat_iteration++;
        results->stat_precision = maxresiduum;

        /* exchange m1 and m2 */
        i = m1;
        m1 = m2;
        m2 = i;

        /* check for stopping calculation depending on termination method */
        if (options->termination == TERM_PREC) {
            if (maxresiduum < options->term_precision) {
                term_iteration = 0;
            }
        } else if (options->termination == TERM_ITER) {
            term_iteration--;
        }
    }

    results->m = m2;
}

/* ************************************************************************ */
/*  displayStatistics: displays some statistics about the calculation       */
/* ************************************************************************ */
static void displayStatistics(struct calculation_arguments const* arguments,
                              struct calculation_results const* results,
                              struct options const* options) {
    int N = arguments->N;
    double time =
        (comp_time.tv_sec - start_time.tv_sec) + (comp_time.tv_usec - start_time.tv_usec) * 1e-6;

    printf("Berechnungszeit:    %f s \n", time);
    printf("Speicherbedarf:     %f MiB\n",
           (N + 1) * (N + 1) * sizeof(double) * arguments->num_matrices / 1024.0 / 1024.0);
    printf("Berechnungsmethode: ");

    if (options->method == METH_GAUSS_SEIDEL) {
        printf("Gauß-Seidel");
    } else if (options->method == METH_JACOBI) {
        printf("Jacobi");
    }

    printf("\n");
    printf("Interlines:         %" PRIu64 "\n", options->interlines);
    printf("Störfunktion:      ");

    if (options->inf_func == FUNC_F0) {
        printf("f(x,y) = 0");
    } else if (options->inf_func == FUNC_FPISIN) {
        printf("f(x,y) = 2pi^2*sin(pi*x)sin(pi*y)");
    }

    printf("\n");
    printf("Terminierung:       ");

    if (options->termination == TERM_PREC) {
        printf("Hinreichende Genauigkeit");
    } else if (options->termination == TERM_ITER) {
        printf("Anzahl der Iterationen");
    }

    printf("\n");
    printf("Anzahl Iterationen: %" PRIu64 "\n", results->stat_iteration);
    printf("Norm des Fehlers:   %e\n", results->stat_precision);
    printf("\n");
}

/**
 * rank and size are the MPI rank and size, respectively.
 * from and to denote the global(!) range of lines that this process is responsible for.
 *
 * Example with 9 matrix lines and 4 processes:
 * - rank 0 is responsible for 1-2, rank 1 for 3-4, rank 2 for 5-6 and rank 3 for 7.
 *   Lines 0 and 8 are not included because they are not calculated.
 * - Each process stores two halo lines in its matrix (except for ranks 0 and 3 that only store
 * one).
 * - For instance: Rank 2 has four lines 0-3 but only calculates 1-2 because 0 and 3 are halo lines
 * for other processes. It is responsible for (global) lines 5-6.
 */

static void displayMatrix(struct calculation_arguments* arguments,
                          struct calculation_results* results, struct options* options) {
    int const elements = 8 * options->interlines + 9;

    int x, y;
    double** Matrix = arguments->Matrix[results->m];
    MPI_Status status;
    // gets rank ... from arguments
    int rank = arguments->rank;
    int size = arguments->size;
    int from = arguments->from;
    int to = arguments->to;
    /* first line belongs to rank 0 */
    if (rank == 0)
        from--;

    /* last line belongs to rank size - 1 */
    if (rank + 1 == size)
        to++;

    if (rank == 0)
        printf("Matrix:\n");

    for (y = 0; y < 9; y++) {
        int line = y * (options->interlines + 1);

        if (rank == 0) {
            /* check whether this line belongs to rank 0 */
            if (line < from || line > to) {
                /* use the tag to receive the lines in the correct order
                 * the line is stored in Matrix[0], because we do not need it anymore */
                MPI_Recv(Matrix[0], elements, MPI_DOUBLE, MPI_ANY_SOURCE, 42 + y, MPI_COMM_WORLD,
                         &status);
            }
        } else {
            if (line >= from && line <= to) {
                /* if the line belongs to this process, send it to rank 0
                 * (line - from + 1) is used to calculate the correct local address */
                MPI_Send(Matrix[line - from + 1], elements, MPI_DOUBLE, 0, 42 + y, MPI_COMM_WORLD);
            }
        }

        if (rank == 0) {
            for (x = 0; x < 9; x++) {
                int col = x * (options->interlines + 1);

                if (line >= from && line <= to) {
                    /* this line belongs to rank 0 */
                    printf("%7.4f", Matrix[line][col]);
                } else {
                    /* this line belongs to another rank and was received above */
                    printf("%7.4f", Matrix[0][col]);
                }
            }

            printf("\n");
        }
    }

    fflush(stdout);
}

/* ************************************************************************ */
/*  main                                                                    */
/* ************************************************************************ */
int main(int argc, char** argv) {
    struct options options;
    struct calculation_arguments arguments;
    struct calculation_results results;
    MPI_Init(&argc, &argv);
    // Struct Data Type:
    MPI_Datatype types[2] = {MPI_INT64_T, MPI_DOUBLE};
    int lengths[2] = {6, 1};
    MPI_Aint displacement[2];
    MPI_Get_address(&options.number, displacement);
    MPI_Get_address(&options.term_precision, &displacement[1]);
    displacement[1] -= displacement[0];
    displacement[0] = 0;

    MPI_Datatype options_type;
    MPI_Type_create_struct(2, lengths, displacement, types, &options_type);
    MPI_Type_commit(&options_type);

    MPI_Comm_rank(MPI_COMM_WORLD, &arguments.rank);
    // Abfrage der Eingabe
    if (!arguments.rank)
        askParams(&options, argc, argv);
    // Send Options to other prozesses
    MPI_Bcast(&options, 1, options_type, 0, MPI_COMM_WORLD);

    if (options.method == METH_JACOBI)
        MPI_Comm_size(MPI_COMM_WORLD, &arguments.size);
    else
        // Gaus-Seidel sequenziell
        arguments.size = 1;

    initVariables(&arguments, &results, &options);

    allocateMatrices(&arguments);
    initMatrices(&arguments, &options);
    MPI_Barrier(MPI_COMM_WORLD);
    // Timing in Prozess 0
    if (!arguments.rank)
        gettimeofday(&start_time, NULL);
    // bei größe 1 sequenziell
    if (arguments.size == 1)
        calculate_old(&arguments, &results, &options);
    else
        calculate_parallel_jacobi(&arguments, &results, &options);
    MPI_Barrier(MPI_COMM_WORLD);
    if (!arguments.rank) {
        gettimeofday(&comp_time, NULL);
        displayStatistics(&arguments, &results, &options);
    }

    displayMatrix(&arguments, &results, &options);

    freeMatrices(&arguments);
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
    return 0;
}

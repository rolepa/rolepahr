#define RESET "\033[00m"
#define BOLD "\033[1m"
#define DARK "\033[2m"
#define UNDERLINE "\033[4m"
#define BLINK "\033[5m"
#define REVERSE "\033[7m"
#define CONCEALED "\033[8m"
#define GRAY "\033[30m"
#define GREY "\033[30m"
#define RED "\033[31m"
#define GREEN "\033[32m"
#define YELLOW "\033[33m"
#define BLUE "\033[34m"
#define MAGENTA "\033[35m"
#define CYAN "\033[36m"
#define WHITE "\033[37m"
#define BG_GRAY "\033[40m"
#define BG_GREY "\033[40m"
#define BG_RED "\033[41m"
#define BG_GREEN "\033[42m"
#define BG_YELLOW "\033[43m"
#define BG_BLUE "\033[44m"
#define BG_MAGENTA "\033[45m"
#define BG_CYAN "\033[46m"
#define BG_WHITE "\033[47m"

#define TBOLD(text) "\033[1m" text RESET
#define TDARK(text) "\033[2m" text RESET
#define TUNDERLINE(text) "\033[4m" text RESET
#define TBLINK(text) "\033[5m" text RESET
#define TREVERSE(text) "\033[7m" text RESET
#define TCONCEALED(text) "\033[8m" text RESET
#define TGRAY(text) "\033[30m" text RESET
#define TGREY(text) "\033[30m" text RESET
#define TRED(text) "\033[31m" text RESET
#define TGREEN(text) "\033[32m" text RESET
#define TYELLOW(text) "\033[33m" text RESET
#define TBLUE(text) "\033[34m" text RESET
#define TMAGENTA(text) "\033[35m" text RESET
#define TCYAN(text) "\033[36m" text RESET
#define TWHITE(text) "\033[37m" text RESET
#define TBG_GRAY(text) "\033[40m" text RESET
#define TBG_GREY(text) "\033[40m" text RESET
#define TBG_RED(text) "\033[41m" text RESET
#define TBG_GREEN(text) "\033[42m" text RESET
#define TBG_YELLOW(text) "\033[43m" text RESET
#define TBG_BLUE(text) "\033[44m" text RESET
#define TBG_MAGENTA(text) "\033[45m" text RESET
#define TBG_CYAN(text) "\033[46m" text RESET
#define TBG_WHITE(text) "\033[47m" text RESET
##Pseudocode

/* ************************************************************************ */
/* calculate: solves the equation                                           */
/* ************************************************************************ */
ccalculate_parallel_gauss(struct calculation_arguments const* arguments,
                                     struct calculation_results* results,
                                     struct options const* options){
    int i, j                /* local variables for loops */
    double star             /* four times center value minus 4 neigh.b values */
    double residuum         /* residuum of current iteration */
    double maxresiduum = 0  /* maximum residuum value of a slave in iteration */
    double maxresiduum_last /* maximum residuum value received of rank-1 */
    double maxresiduum_next /* maximum residuum value passed to rank+1 */
    MPI_Request maxresrq = 0
    MPI_Request lastline_resvrq = 0
    MPI_Request firstline_sendrq = 0
    unsigned rank = arguments->rank
    unsigned size = arguments->size
    int const N = arguments->N
    int const height = arguments->height
    double const h = arguments->h
    // immer 0 weil gauss
    results->m = 0

    double pih = 0.0
    double fpisin = 0.0
    double** Matrix = arguments->Matrix[0]

    int term_iteration = options->term_iteration

    if options->inf_func == FUNC_FPISIN
        pih = PI * h
        fpisin = 0.25 * TWO_PI_SQUARE * h * h
    

    while term_iteration > 0
        // Alle außer Rang 0
        if rank
            if options->termination == TERM_PREC
                MPI_Recv(&maxresiduum_last, 1, MPI_DOUBLE, rank - 1, ID_MAXRES, MPI_COMM_WORLD,
                         MPI_STATUS_IGNORE)
                if maxresiduum_last < 0
                    if rank < size - 1
                        MPI_Send(&maxresiduum_last, 1, MPI_DOUBLE, rank + 1, ID_MAXRES,
                                 MPI_COMM_WORLD)
                    MPI_Allreduce(MPI_IN_PLACE, &maxresiduum, 1, MPI_DOUBLE, MPI_MAX,
                                  MPI_COMM_WORLD)
                    return
                 else
                    maxresiduum = maxresiduum_last
            
            // Empfängt Vorgänger-Zeile
            MPI_Recv(Matrix[0] + 1, N, MPI_DOUBLE, rank - 1, ID_DATA, MPI_COMM_WORLD,
                     MPI_STATUS_IGNORE)
            // Rang 0 aber erst wenn auch der letzte das erstemal gerechnet hat
         else if options->termination == TERM_PREC && results->stat_iteration >= size
            MPI_Recv(&maxresiduum, 1, MPI_DOUBLE, size - 1, ID_MAXRES, MPI_COMM_WORLD,
                     MPI_STATUS_IGNORE)
            if maxresiduum < options->term_precision
                maxresiduum_next = -1
                MPI_Send(&maxresiduum_next, 1, MPI_DOUBLE, rank + 1, ID_MAXRES, MPI_COMM_WORLD)
                MPI_Allreduce(MPI_IN_PLACE, &maxresiduum, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD)
                results->stat_precision = maxresiduum
                return
            
        
        if results->stat_iteration && rank != size - 1
            MPI_Irecv(Matrix[height - 1] + 1, N, MPI_DOUBLE, rank + 1, ID_DATA, MPI_COMM_WORLD,
                      &lastline_resvrq)

        if firstline_sendrq
            MPI_Wait(&firstline_sendrq, MPI_STATUS_IGNORE)
        if !rank
            maxresiduum = 0
        /* over all rows in prozess*/
        for i = 1 i < height - 1 i++
            // Wartet auf erste Zeile vom Vorgänger
            if i == height - 2 && results->stat_iteration && rank != size - 1
                MPI_Wait(&lastline_resvrq, MPI_STATUS_IGNORE)
            double fpisin_i = 0.0

            if options->inf_func == FUNC_FPISIN
                // needs to use the global line index
                fpisin_i = fpisin * sin(pih * (doublei + arguments->from - 1

            /* over all columns */
            for j = 1 j < N j++
                star = 0.25 *
                       (Matrix[i - 1][j] + Matrix[i][j - 1] + Matrix[i][j + 1] + Matrix[i + 1][j])

                if options->inf_func == FUNC_FPISIN
                    star += fpisin_i * sin(pih * (double)

                if options->termination == TERM_PREC || term_iteration == 1
                    residuum = Matrix[i][j] - star
                    residuum = (residuum < 0? -residuum : residuum
                    maxresiduum = (residuum < maxresiduum? maxresiduum : residuum
                

                Matrix[i][j] = star
            
            // Sendet erste Zeile an Vorgänger
            if rank && i == 1
                MPI_Isend(Matrix[1] + 1, N, MPI_DOUBLE, rank - 1, ID_DATA, MPI_COMM_WORLD,
                          &firstline_sendrq)
        
        // Sendet letzte Zeile an Nachfolger
        if rank < size - 1
            MPI_Isend(Matrix[height - 2] + 1, N, MPI_DOUBLE, rank + 1, ID_DATA, MPI_COMM_WORLD,
                      &lastline_resvrq)

        // Synchronisiert maxresiduum
        if options->termination == TERM_PREC
            if maxresrq
                MPI_Wait(&maxresrq, MPI_STATUS_IGNORE)
            maxresiduum_next = maxresiduum
            MPI_Isend(&maxresiduum_next, 1, MPI_DOUBLE, rank < size - 1 ? rank + 1 : 0, ID_MAXRES,
                      MPI_COMM_WORLD, &maxresrq)
        

        results->stat_iteration++
        /* check for stopping calculation depending on termination method */
        if options->termination == TERM_ITER
            term_iteration--
    
    MPI_Allreduce(MPI_IN_PLACE, &maxresiduum, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD)
    results->stat_precision = maxresiduum
}
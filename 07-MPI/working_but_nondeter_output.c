#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

int* init(int N, int size, int rank) {
    // TODO
    int* buf = malloc(sizeof(int) * size);

    srand(time(NULL) + rank);
    for (int i = 0; i < size; i++) {
        if (N > i)
            // Do not modify "% 25"
            buf[i] = rand() % 25;
        else
            buf[i] = -1;
    }
    return buf;
}

int* circle(int* buf, int size, int rank, int nprocs) {
    MPI_Request test = 0;
    MPI_Isend(buf, size, MPI_INT, (rank + 1) >= nprocs ? 0 : rank + 1, 0,
              MPI_COMM_WORLD, &test);
    int* ret_buf = malloc(sizeof(int) * size);
    MPI_Recv(ret_buf, size, MPI_INT, rank ? rank - 1 : nprocs - 1, 0,
             MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    return ret_buf;
}

int n_rank(int N, int rank, int nprocs) {
    return N / nprocs +
           (((rank % nprocs) + nprocs) % nprocs < N % nprocs ? 1 : 0);
}
int size(int N, int nprocs) {
    return N / nprocs + 1;
}
int done(int* buf, int start_value, int nprocs) {
    int done = buf[0] == start_value;
    MPI_Bcast(&done, 1, MPI_INT, nprocs - 1, MPI_COMM_WORLD);
    return done;
}

int main(int argc, char** argv) {
    int N;
    int rank;
    int* buf;
    int nprocs;
    int iter = 0;
    int start_value = 0;
    if (argc < 2) {
        printf("Arguments error!\nPlease specify a buffer size.\n");
        return EXIT_FAILURE;
    }

    // Array length
    N = atoi(argv[1]);
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    buf = init(n_rank(N, rank, nprocs), size(N, nprocs), rank);

    // TODO
    if (rank == 0) {
        printf("\nBEFORE\n");
        MPI_Request rq = 0;
        MPI_Isend(buf, 1, MPI_INT, nprocs - 1, 0, MPI_COMM_WORLD, &rq);
    }
    if (rank == nprocs - 1) {
        MPI_Recv(&start_value, 1, MPI_INT, 0, 0, MPI_COMM_WORLD,
                 MPI_STATUS_IGNORE);
    }
    for (int r = 0; r < nprocs; r++) {
        if (r == rank) {
            printf("rank %d:\n[ ", rank);
            for (int j = 0; j < n_rank(N, rank, nprocs); j++) {
                printf("%d ", buf[j]);
            }
            printf("]\n");
        }
        MPI_Barrier(MPI_COMM_WORLD);
    }
    while (!done(buf, start_value, nprocs)) {
        buf = circle(buf, size(N, nprocs), rank, nprocs);
        iter++;
    }
    fflush(stdout);
    MPI_Barrier(MPI_COMM_WORLD);
    if (rank == 0) {
        printf("\nAFTER\n");
    }
    fflush(stdout);
    MPI_Barrier(MPI_COMM_WORLD);

    for (int r = 0; r < nprocs; r++) {
        if (r == rank) {
            printf("iter %d, rank %d:\n[ ", iter, rank);
            for (int j = 0; j < n_rank(N, rank - iter, nprocs); j++) {
                printf("%d ", buf[j]);
            }
            printf("]\n");
        }
        MPI_Barrier(MPI_COMM_WORLD);
    }

    MPI_Finalize();
    return EXIT_SUCCESS;
}

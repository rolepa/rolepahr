#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int* init(int N, int size, int rank) {
    // TODO
    int* buf = malloc(sizeof(int) * size);

    srand(time(NULL) + rank);
    for (int i = 0; i < size; i++) {
        if (N > i)
            // Do not modify "% 25"
            buf[i] = rand() % 25;
        else
            buf[i] = -1;
    }
    return buf;
}

int* circle(int* buf, int size, int rank, int nprocs) {
    MPI_Request test = 0;
    MPI_Isend(buf, size, MPI_INT, (rank + 1) >= nprocs ? 0 : rank + 1, 0,
              MPI_COMM_WORLD, &test);
    int* ret_buf = malloc(sizeof(int) * size);
    MPI_Recv(ret_buf, size, MPI_INT, rank ? rank - 1 : nprocs - 1, 0,
             MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    return ret_buf;
}
// Berechnet die Anzahl an Einträgen
int n_rank(int N, int rank, int nprocs) {
    return N / nprocs +
           (((rank % nprocs) + nprocs) % nprocs < N % nprocs ? 1 : 0);
}
// Berechnet die maximale Anzahl an Einträgen
int size(int N, int nprocs) {
    return N / nprocs + 1;
}
// Überprüft ob Circeln fertig ist
int done(int* buf, int start_value, int nprocs) {
    int done = buf[0] == start_value;
    MPI_Bcast(&done, 1, MPI_INT, nprocs - 1, MPI_COMM_WORLD);
    return done;
}

int main(int argc, char** argv) {
    int N;
    int rank;
    int* buf;
    int nprocs;
    int iter = 0;
    int start_value = 0;
    int message_length;
    char* message;
    if (argc < 2) {
        printf("Arguments error!\nPlease specify a buffer size.\n");
        return EXIT_FAILURE;
    }

    // Array length
    N = atoi(argv[1]);
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    message_length = (10 + 5 + n_rank(N, 0, nprocs) * 3 + 2);
    message = malloc(sizeof(char) * message_length);
    buf = init(n_rank(N, rank, nprocs), size(N, nprocs), rank);

    // Senden der Endbedingung
    if (rank == 0) {
        printf("\nBEFORE\n");
        MPI_Send(buf, 1, MPI_INT, nprocs - 1, 0, MPI_COMM_WORLD);
    }
    if (rank == nprocs - 1) {
        MPI_Recv(&start_value, 1, MPI_INT, 0, 0, MPI_COMM_WORLD,
                 MPI_STATUS_IGNORE);
    }
    // Baut Ausgabestring
    sprintf(message, "rank %d:\n[ ", rank);
    for (int j = 0; j < n_rank(N, rank, nprocs); j++) {
        sprintf(message + strlen(message), "%d ", buf[j]);
    }
    sprintf(message + strlen(message), "]\n");
    // Ausgabe über Prozess 0
    if (rank)
        MPI_Send(message, message_length, MPI_CHAR, 0, 0, MPI_COMM_WORLD);
    else {
        printf(message);
        for (int i = 1; i < nprocs; i++) {
            MPI_Recv(message, message_length, MPI_CHAR, i, 0, MPI_COMM_WORLD,
                     MPI_STATUS_IGNORE);
            printf(message);
        }
    }
    // Eigentliche Berechnung
    while (!done(buf, start_value, nprocs)) {
        buf = circle(buf, size(N, nprocs), rank, nprocs);
        iter++;
    }
    MPI_Barrier(MPI_COMM_WORLD);
    if (rank == 0) {
        printf("\nAFTER\n");
    }
    
    // Baut Ausgabestring
    sprintf(message, "rank %d:\n[ ", rank);
    for (int j = 0; j < n_rank(N, rank - iter, nprocs); j++) {
        sprintf(message + strlen(message), "%d ", buf[j]);
    }
    sprintf(message + strlen(message), "]\n");
    // Ausgabe über Prozess 0
    if (rank)
        MPI_Send(message, message_length, MPI_CHAR, 0, 0, MPI_COMM_WORLD);
    else {
        printf("%d iterations\n", iter);
        printf(message);
        for (int i = 1; i < nprocs; i++) {
            MPI_Recv(message, message_length, MPI_CHAR, i, 0, MPI_COMM_WORLD,
                     MPI_STATUS_IGNORE);
            printf(message);
        }
    }

    free(message);
    MPI_Finalize();
    return EXIT_SUCCESS;
}

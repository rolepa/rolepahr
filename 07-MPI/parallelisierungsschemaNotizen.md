---
geometry: margin=2cm
output: pdf_document
lang: de
---
# Parallelisierung mit MPI
## Datenaufteilung
Allgemein wird unser Matrix in Zeilen aufgeteilt und berechnet, sodass jeder verfügbare Prozess eine Zeile der Matrix bekommt.
Dabei muss jeder Prozess bis auf der Erste auf die Ergebnisse seines Vorgängers warten muss, die er für seine Rechnung braucht.
Diese werden per Nachricht weitergegeben. Dabei hält jeder Prozess, die Zeilen die er berechnet im Speicher, bzw. erzeugt diese, die Zeilen darunter sendet der Nachfolgende Prozess, sobald er fertig ist, die darüberliegenden Zellen werden bei Jacobi Abschnittweise gesendet.

Da bei Gauß-Seidel die berechneten Ergebnisse erst in der nächsten Iterration berücksichtigt werden müssen, hält hier jeder Prozess mehrere Zeilen auf einmal siehe Abb. \ref{gauss}, vergleichbar mit den bisherigen Implementationen in OpenMP und PThreads, und gibt die Grenzen an die Nachbar Prozesse weiter. 

![Aufteilung bei 3 Prozessen, einer 5x5 Matrix\label{gauss}](gauss.png){ width=60% }

## Parallelisierungsschema Jacobi
Bei $N=1$, dargestellt in Abb. \ref{one}, berechnet Prozess 1, 1,1 der Matrix und wenn er fertig ist sendet er nonblocking an Prozess 2 das Ergebnis.
Daraufhin startet Prozess 2 mit 1,2 und sendet bei Fertigstellung das Ergebnis davon an den dritten Prozess, und bekommt das Ergebnis für 2,1 von Prozess 1. 
Dies wird dann diagonal fortgesetzt, bis die Prozesse am rechten Rand der Matrix ankommen. Dann springen diese in die nächste "freie" Zeile, und berechnen dort wieder diagonal, bei 4 Prozessen berechnet der erste also 1, 5, 9, 13..., der zweite 2, 6, 10, 14... und so weiter. 
Der letzte Prozess sendet seine Ergebnisse an den ersten, solange bis einer die letzte Zeile berechnet.
Für $N\ge2$ passiert das gleiche, nur das jeder Prozess, dann $N$ Einträge auf einmal berechnet, d.h. bei Schritt 1, berechnet der erste Prozess $\{(1,1) \dots (N,1)\}$, in Schritt 2, der erste Prozess $\{(N+1,1) \dots (2N,1)\}$ und der zweite $\{(1,2) \dots (N,2)\}$. Dargestellt für $N=2$ in Abb. \ref{two}.

![Aufteilung mit N=1, 3 Prozessen, einer 5x5 Matrix, die Farben sind die parallelen Berechnungen\label{one}](aufteilung1.png){ width=60% }

![Aufteilung mit N=2, 3 Prozessen, einer 5x5 Matrix, die Farben sind die parallelen Berechnungen\label{two}](aufteilung2.png){ width=60% }

## Parallelisierungsschema Gauß-Seidel

Datenaufteilung ist wie in OpenMP erfolgt, die einzelnen Prozesse, berechnen ihre Zeilen zeilenweise und senden nonblocking, wenn damit fertig, ihr Ergebnis an den Folgeprozess, damit dieser in der nächsten Iteration darauf zugreifen kann. Die Prozesse könnten dabei mit der ersten Spalte warten, damit der sendende Prozess mehr Zeit hat. Wenn sie mit ihrer letzten Zeile anfangen, können sie diese schneller an den Folgeprozess senden.

Es kann sinnvoll sein, das jeder Prozess, seine Teilmatrix zwei Mal im Speicher hält, so dass er weiterrechnen kann, bevor fest steht ob die Abbruchbedingung nach Genauigkeit erfüllt ist.

## Abbruchbedingung
### Iterationenzahl
Für den Abbruch nach Iterationenzahl hält jeder Prozess an, wenn die Iterationenzahl erreicht ist und wartet auf die anderen Prozesse.
Je nach Ausgabe, kann er mit dieser auch bereits beginnen.

### Genauigkeit
Bei Abbruch nach Genauigkeit, berechnet jeder Prozess ein Maxresiduum, welches anschließen mit einem `Nonblocking All-Reduce` gesammelt wird, so dass die Prozesse weiter rechnen können, jedoch müssen sie solange nicht feststeht, dass die Genauigkeit nicht erfüllt ist, nur wenn Cache vorhanden ist weiter rechnen und sie müssen regelmäßig überprüfen, ob das `Reduce` fertig ist. 
Und dann entweder weiterrechnen oder aufhören, und das letzte Ergebnis ausgeben.
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include "mpi.h"
int main(int argc, char *argv[])
{
    int rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    char message[40]="";
    int* spalten[2];
    int zahlen[6] = {4,8,15,16,23,42};
    spalten[0]=zahlen;
    int zahlen2[6] = {5,4,1,12,20,30};
    spalten[1]=zahlen2;
    
    if (rank)
    {
        gethostname(message, 10);
        struct timeval tv;
        struct tm *tm;
        gettimeofday(&tv, NULL);
        tm = localtime(&tv.tv_sec);
        strftime(message + strlen(message), 30, ": %Y-%m-%d %H:%M:%S.", tm);
        sprintf(message + strlen(message), "%d", (int)tv.tv_usec);
        MPI_Send(message, 40, MPI_CHAR, 0, 0, MPI_COMM_WORLD);
    }
    else
    {
        int size;
        MPI_Status status;
        MPI_Comm_size(MPI_COMM_WORLD, &size);
        for (int i = 1; i < size; i++)
        {
            MPI_Recv(message, 40, MPI_CHAR, i, 0, MPI_COMM_WORLD, &status);
            printf("%s\n", message);
        }
        // Wir hatten einmal den Fall, das eine Beendet-Nachricht vor allen Zeitstempeln kam
        fflush(stdout);
    }
    MPI_Barrier(MPI_COMM_WORLD);
    printf("Rang %d beendet jetzt!\n", rank);
    MPI_Finalize();
    return 0;
}

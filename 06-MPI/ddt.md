---
geometry: margin=2cm
output: pdf_document
---
# Abgabe 6
## Wie kann man in DDT die Programmparameter angeben? 
Im Eingabefeld "Arguments" beim Programm start:

![la lune](imgs/Programmparameter.png)

### Gibt es dafür auch andere Möglichkeiten? Wenn ja, welche?
Wir haben keine weitern gefunden.
\newpage

## Welche Step-Möglichkeiten gibt es und wie unterscheiden sich diese?

Es gibt 3(4) verschiedene Step-Möglichkeiten:

1. Step Into: Springt wenn vorhanden in die nächste Funktion in der Zeile.
2. Step Over: Führt die nächste Zeile aus.
3. Step Out: Führt aktuelle Funktion bis zum Ende aus und wartet dann.
4. Run To Line: Lässt den Code bis zur angegebenen Line laufen.

## Schauen Sie sich die Werte der Variable an, in der Sie den Rang des aktuellen Prozesses gespeichert haben. Was fällt Ihnen in der Darstellung auf?
![la lune](imgs/Variablen.png)

Bei den Variablen wird mit horizontalen Strichen davor angezeigt, wie die Werte der anderen Prozesse sind:

- Wenn diese auf einer Höhe sind, dann sind die Werte von den Threads gleich.
- Bei unterschiedlicher Höhe, kann man das Größenverhältnis abschätzen. Die Striche sind dabei nach den Prozess-IDs sortiert.
- Bei nicht initialisierten Werten sind die Striche des Prozesses Rot.

## Evaluate Fenster

Das Evaluate Fenster bietet Möglichkeiten um in die Werte der Variablen reinschauen zu können, aber auch um Ausdrücke zu berechnen.

![la lune](imgs/Evaluate_0.png)
\newpage

### Prozesswechsel
Beim Wechseln des Prozesses, werden alle Ausdrücke neu berechnet.

![la lune](imgs/Evaluate_1.png)

## Welche sonstigen Visualisierungsmöglichkeiten bietet DDT?
Darstellung der Werte:

- 3D Diagramm:

    ![la lune](imgs/Diagram2.png){ width=70% }

    Wobei auch mehrdimmensionale Arrays angezeigt werden können.
\newpage

- Als Tabelle:

    ![la lune](imgs/Tabelle.png){ width=70% }
- Statistik:

    ![la lune](imgs/Array_Stat.png){ width=70% }

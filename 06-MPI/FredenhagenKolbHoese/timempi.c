#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include "mpi.h"
int main(int argc, char *argv[])
{
    int rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    // Speichert die ausgegebene Meldung, zum Senden und Empfangen.
    char message[40]="";
    if (rank)
    {
        gethostname(message, 10);
        struct timeval tv;
        struct tm *tm;
        // Fragt die aktuelle Zeit ab
        gettimeofday(&tv, NULL);
        // Formartiert Zeit aber leider keine µs
        tm = localtime(&tv.tv_sec);
        strftime(message + strlen(message), 30, ": %Y-%m-%d %H:%M:%S.", tm);
        // Fügt den µs Teil an
        sprintf(message + strlen(message), "%d", (int)tv.tv_usec);
        // Die 1. 0 ist der Thread mit ID 0, die 2. ist die ID der Übertragung
        MPI_Send(message, 40, MPI_CHAR, 0, 0, MPI_COMM_WORLD);
    }
    else
    {
        int size;
        MPI_Status status;
        MPI_Comm_size(MPI_COMM_WORLD, &size);
        for (int i = 1; i < size; i++)
        {
            // Hier wird dann über alle Threads iteriert, wobei die ID der Übertragung wie oben 0 ist.
            MPI_Recv(message, 40, MPI_CHAR, i, 0, MPI_COMM_WORLD, &status);
            printf("%s\n", message);
        }
        // Wir hatten einmal den Fall, das eine Beendet-Nachricht vor einigen Zeitstempeln kam
        fflush(stdout);
    }
    MPI_Barrier(MPI_COMM_WORLD);
    // Alle Threads sind fertig
    printf("Rang %d beendet jetzt!\n", rank);
    MPI_Finalize();
    return 0;
}

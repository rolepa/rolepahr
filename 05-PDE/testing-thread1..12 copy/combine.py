import re
import os
times=[]
for d in os.listdir("."):
    if os.path.isdir(d):
        with open(d+"/output.test", "r") as myfile:
            lines = myfile.readlines()

        times.append([])
        for line in lines:
            match = re.match("Berechnungszeit:\s*(\d*\.\d*)", line)
            if match:
                times[-1].append(match.group(1))
out=""
if len(times)>0:
    for i in range(len(times[0])):
        for time in times:
            out+=time[i]+"  "
        out+="\n"

print(out)

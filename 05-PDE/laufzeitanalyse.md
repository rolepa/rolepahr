---
output: pdf_document
---

# Abgabe 5

## Parallelisierung mit POSIX-Threads

Nach der Anpassung von Makefile und pardiff.c, zur zeilenweisen Parallelisierung, d.h. jeder Thread berechnet eine Zeile bis alle Zeilen berechnet sind, konnten wir bei einer Messung mit 12 Threads, den geforderten Beschleunigungsfaktor von 10 feststellen.

Zeiten in s bei 512 Interlines und 600 Iterationen:

Host | Sequenziell | Parallel
---- |----------- | -------- 
amd2 |670.25      | 62.75
amd3 |672.69      | 62.47
amd4 |677.59      | 62.58

Wobei die Tests alternierend auf einem Knoten ausgeführt wurden um möglichst akkurate Testergebnisse zu erhalten. Die durchschnittliche Dauer wurde so um einen Faktor von 10.76 reduziert.

Auf `west` hatten wir einen kleineren Geschwindigkeitsgewinn (ca. Faktor 7) gemessen, vieleicht gibt es Unterschiede in der Prozessorarchitektur.

Bearbeitung ca. 2.5h
\newpage
 
## Leistungsanalyse
In der ersten Messung, haben wir die Leistungssteigerung bei wachsender Threadzahl verglichen.

Dafür haben wir 512 Interlines bei 600 Iterationen verwendet.

![la lune](speedup.svg){ width=70% }

Dargestellt sind gemittelte Werte von jeweils 3 Messungen (amd2-4).

Hier sieht man, dass der Jacobi-Algorithmus sehr gut parallelisierbar ist. Da jede Zeile unabhängig berechnet werden kann und daher nur das Erzeugen der Matrix und die Rückgabe sequenziell laufen. Beides im Vergleich zur Berechnung schnelle Prozesse.

Je schneller die `calculate()` Funktion jedoch ausgeführt wird, desto größer wird der Anteil, den diese sequenziellen Prozesse ausmachen. Auch kann die Aufteilung der Zeilen im vorherein dazu führen, dass einige Threads vor anderen terminieren, was wahrscheinlicher bei höherer Threadzahl und kleineren Aufgabenbereichen ist. Zum Beispiel haben 2 Threads jeweils 6 Mal so viele Zeilen zu berechnen wie 12, dadurch fallen einzelne schnellere Rechnungen weniger ins Gewicht.


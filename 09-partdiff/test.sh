mv partdiff-par partdiff
mpirun -np 20 --oversubscribe ./partdiff  1 1  0 1 2 5
mpirun -np  3 --oversubscribe ./partdiff 20 1  5 1 2 5
mpirun -np  1 --oversubscribe ./partdiff  5 1 10 2 1 1e-4 
mpirun -np 40 --oversubscribe ./partdiff 70 1 50 2 1 1e-4 
mpirun -np 15 --oversubscribe ./partdiff 10 2  0 2 2 10
mpirun -np  1 --oversubscribe ./partdiff 20 2 10 2 2 30 
mv partdiff partdiff-par
